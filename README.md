# Jeu de la vie

Dans ce projet j'aimerais faire un petit jeu de la vie qui est un « jeu à zéro joueur », puisqu'il ne nécessite pas l'intervention du joueur lors de son déroulement.
Le jeu de la vie est un automate cellulaire , l'état de l'automate à l'étape n depend de son état à l'étape n - 1.

L'évolution de l'état d'une cellule dépend de l'état de ses 8 plus proches voisines : 
- si une cellule a exactement trois voisines vivantes, elle est vivante à l’étape suivante.
- si une cellule a exactement deux voisines vivantes, elle reste dans son état actuel à l’étape suivante.
- si une cellule a strictement moins de deux ou strictement plus de trois voisines vivantes, elle est morte à l’étape suivante.

Pour cela je vais me servir de la bibliotheque minifb pour ouvrir une fenetre qui contiendra le jeu .

les règles du jeu en détail sont décrites <a href="https://fr.wikipedia.org/wiki/Jeu_de_la_vie#R%C3%A8gles">Ici</a>.


# Fonctionnement 
- Lancez le jeu 
- Cliquez sur le **clic droit** de la souris pour activer des cellules.
- Appuyez sur la touche du clavier **Enter** pour mettre à jour les etats des cellules. 
- Appuyez sur la touche du clavier **Escape** pour quitter le jeu.

