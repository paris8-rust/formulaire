extern crate minifb;
use anyhow::Result;
use minifb::{Key, MouseButton, MouseMode, Window, WindowOptions};
use std::mem;
use std::time::SystemTime;
const TAILLE_CELL: usize = 16;
const TAILLE_GRILLE: usize = 50;

const WIDTH: usize = TAILLE_CELL * TAILLE_GRILLE;
const HEIGHT: usize = TAILLE_CELL * TAILLE_GRILLE;

const C_W: usize = TAILLE_GRILLE;
const C_H: usize = TAILLE_GRILLE;

struct Grille {
    ///etat cellule à l'instant t
    cellule_initiale: Vec<bool>,
    ///etat cellule à l'étape suivante
    cellule_suivante: Vec<bool>,
}
impl Grille {
    fn new(cellule_initiale: Vec<bool>, cellule_suivante: Vec<bool>) -> Grille {
        Grille {
            cellule_initiale,
            cellule_suivante,
        }
    }
    // fonction qui permet d'échanger des valeurs
    fn permutation(&mut self) {
        mem::swap(&mut self.cellule_initiale, &mut self.cellule_suivante);
    }
    fn obtenir_valeur_cellule(&self, x: usize, y: usize) -> bool {
        if !Grille::toujours_dans_grille(x, y) {
            return false;
        }
        self.cellule_initiale[(y * TAILLE_GRILLE) + x]
    }
    fn obtenir_xy(value: usize, x: &mut usize, y: &mut usize) {
        *x = value % TAILLE_GRILLE;
        *y = value / TAILLE_GRILLE;
    }
    //indique si x et y ne sont pas sorties de la grille
    fn toujours_dans_grille(x: usize, y: usize) -> bool {
        x < TAILLE_GRILLE && y < TAILLE_GRILLE
    }
    //fonction qui compte le nombre de voisin
    fn nombre_voisin(&self, x: usize, y: usize) -> u32 {
        let mut cmp: u32 = 0;
        if self.obtenir_valeur_cellule(x.wrapping_sub(1), y.wrapping_sub(1)) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x, y.wrapping_sub(1)) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x.wrapping_add(1), y.wrapping_sub(1)) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x.wrapping_sub(1), y) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x.wrapping_add(1), y) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x.wrapping_sub(1), y.wrapping_add(1)) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x, y.wrapping_add(1)) {
            cmp += 1;
        }
        if self.obtenir_valeur_cellule(x.wrapping_add(1), y.wrapping_add(1)) {
            cmp += 1;
        }
        cmp
    }
    //fonction qui permet de mettre à jour l'etat d'une cellule
    fn update(&mut self) {
        for (i, _) in self.cellule_initiale.iter().enumerate() {
            let mut x = 0usize;
            let mut y = 0usize;
            Grille::obtenir_xy(i, &mut x, &mut y);
            self.cellule_suivante[i] = match self.nombre_voisin(x, y) {
                2 => self.cellule_initiale[i], //si elle a 2 voisin elle reste vivante
                3 => true,                     //si elle a 3 voisin elle devient vivante
                _ => false,
            };
        }
        self.permutation();
    }
}
fn game_of_life() -> Result<()> {
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];
    let mut grille = Grille::new(vec![false; C_W * C_H], vec![false; C_W * C_H]);

    grille.update();

    let mut window = Window::new("GAME OF LIFE", WIDTH, HEIGHT, WindowOptions::default())?;

    let mut horloge = SystemTime::now();
    //appuyez sur la touche du clavier Escape pour quitter
    while window.is_open() && !window.is_key_down(Key::Escape) {
        window.get_mouse_pos(MouseMode::Discard).map(|mouse| {
            let cellule_x = mouse.0 as usize / TAILLE_CELL;
            let cellule_y = mouse.1 as usize / TAILLE_CELL;
            //clic gauche de la souris: active les cellules
            if window.get_mouse_down(MouseButton::Left) {
                grille.cellule_initiale[cellule_y * C_W + cellule_x] = true;
            }
            //clic droite de la souris: desactive les cellules
            else if window.get_mouse_down(MouseButton::Right) {
                grille.cellule_initiale[cellule_y * C_W + cellule_x] = false;
            }
        });
        // l'appuie sur la touche du clavier Enter met à jour les etats des cellules au bout de chaque 10 milliseconde
       
            if window.is_key_down(Key::Enter) {
                match horloge.elapsed() {
                    Ok(d) => {
                        if d.as_millis() >= 10u128 {
                            grille.update();
                            horloge = SystemTime::now();
                        }
                    }
                    Err(err) => println!("erreur {}", err),
                }
            }
        

        //pour chaque cellule je dessine un carre
        for (i, cell) in buffer.iter_mut().enumerate() {
            let x = i % WIDTH;
            let y = i / WIDTH;

            let cell_x = x / TAILLE_CELL;
            let cell_y = y / TAILLE_CELL;

            *cell = if grille.cellule_initiale[cell_y * C_W + cell_x] {
                0xFFFFFF
            } else {
                0
            };
        }
        window.update_with_buffer(&buffer, WIDTH, HEIGHT)?;
    }
    Ok(())
}
fn main() -> Result<()> {
    game_of_life()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_toujours_dans_grille() {
        let x = 30;
        let y = 40;
        assert_eq!(Grille::toujours_dans_grille(x, y), true);
    }
    
    #[test]
    fn test_obtenir_valeur_cellule(){
        let x = 0;
        let y = 0;
        let grid = Grille::new(vec![true],vec![true]);
        assert!(&grid.obtenir_valeur_cellule(x,y),true);
    }
}
